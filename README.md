<h1 align="center"> Weather Forecast </h1>

## Tech stack & Libraries
- [Kotlin](https://kotlinlang.org/)
- [Coroutines](https://github.com/Kotlin/kotlinx.coroutines)
- [Flow](https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines.flow/)
- [StateFlow](https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines.flow/-state-flow/index.html)
- [Hilt](https://developer.android.com/training/dependency-injection/hilt-android)
- [JetPack](https://developer.android.com/jetpack)
  - LiveData - Notify domain layer data to views.
  - Lifecycle - Dispose of observing data when lifecycle state changes.
  - [ViewBinding](https://developer.android.com/topic/libraries/view-binding).
  - ViewModel - UI related data holder, lifecycle aware.
- Architecture
  - Multi-module design for the app.
  - MVVM Architecture (View - DataBinding - ViewModel - Model)
  - Clean Architecture approach.
- [Retrofit2 & OkHttp3](https://github.com/square/retrofit) - Construct the REST APIs.
- [Glide](https://github.com/bumptech/glide) - For Loading images from Urls.
- [MockK](https://github.com/mockk/mockk) - Mocking library for Kotlin.
- [Barista](https://github.com/AdevintaSpain/Barista) - Barista makes developing UI test faster, easier and more predictable.

## Architecture
Multi-module application with separation for layers and features with the necessary grouping.
With MVVM architecture with an additional Domain layer for each module by itself.

Modules Design:
- App
- Core:
    - Base
    - Remote
    - UiComponents
- Features:
    - Weather

![architecture](https://developer.android.com/topic/libraries/architecture/images/final-architecture.png)

## Open API
The app uses the [Weatherbit.io](https://www.weatherbit.io/api) for required data.
