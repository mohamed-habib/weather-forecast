package com.android.weather.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.android.weather.R
import com.base.ErrorTypes
import com.base.Resource
import com.base.UserMessage
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsInstanceOf.instanceOf
import org.junit.*
import org.junit.Assert.assertEquals

class DataInputViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    lateinit var viewModel: DataInputViewModel

    @Before
    fun setUp() {
        viewModel = DataInputViewModel()
    }

    @Test
    fun `Given city is empty When validateDataInput Then enter city error is returned`() {
        val city = ""
        val country = ""
        val expectedError =
            Resource.Error<UserMessage>(ErrorTypes.GeneralError(UserMessage(resMessage = R.string.enter_city)))


        viewModel.validateDataInput(city, country)

        assertThat(viewModel.inputValidationFlow.value, instanceOf(Resource.Error::class.java))
        assertEquals(
            expectedError.errorTypes.errorMessage,
            (viewModel.inputValidationFlow.value as Resource.Error).errorTypes.errorMessage
        )
    }

    @Test
    fun `Given city has value and country is null When validateDataInput Then enter country error is returned`() {
        val city = "Cairo"
        val country = null
        val expectedError =
            Resource.Error<UserMessage>(ErrorTypes.GeneralError(UserMessage(resMessage = R.string.enter_country)))

        viewModel.validateDataInput(city, country)

        assertThat(viewModel.inputValidationFlow.value, instanceOf(Resource.Error::class.java))
        assertEquals(
            expectedError.errorTypes.errorMessage,
            (viewModel.inputValidationFlow.value as Resource.Error).errorTypes.errorMessage
        )
    }

    @Test
    fun `Given city and country has values When validateDataInput Then success is returned`() {
        val city = "Cairo"
        val country = "Egypt"

        viewModel.validateDataInput(city, country)

        assertThat(viewModel.inputValidationFlow.value, instanceOf(Resource.Success::class.java))
    }


}