package com.android.weather

import com.android.weather.data.model.Weather
import com.android.weather.data.model.WeatherDetails
import com.android.weather.data.model.WeatherResponse
import com.android.weather.domain.model.WeatherDataItem
import com.android.weather.domain.model.WeatherItem


fun createWeatherItem() = WeatherItem(
    "100F", "22", "0", "",
    "", "Sunday", "02/05/2021"
)

fun createWeatherDetails() = WeatherDetails(
    "100", "22", Weather("", ""), "0",
    1619937298
)

fun createWeatherDataItem() = WeatherDataItem(
    listOf(
        createWeatherItem(),
        createWeatherItem(),
        createWeatherItem(),
        createWeatherItem(),
        createWeatherItem(),
        createWeatherItem(),
        createWeatherItem()
    )
)

fun createWeatherResponse() =
    WeatherResponse(
        listOf(
            createWeatherDetails(),
            createWeatherDetails(),
            createWeatherDetails(),
            createWeatherDetails(),
            createWeatherDetails(),
            createWeatherDetails(),
            createWeatherDetails()
        ), "cairo", "", ""
    )

