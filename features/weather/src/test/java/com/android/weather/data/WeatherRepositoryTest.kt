package com.android.weather.data

import com.android.weather.createWeatherResponse
import com.android.weather.domain.TemperatureUnits
import com.remote.APIResult
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class WeatherRepositoryTest {

    private lateinit var repository: WeatherRepository
    private val dataSource = FakeWeatherDataSource()

    @Before
    fun setUp() {
        repository = WeatherRepository(dataSource)
    }

    @Test
    fun `Given dataSource returns success, When getDailyWeather(), Then success is returned`() =
        runBlockingTest {
            val city = "Cairo"
            val country = "Egypt"
            val days = 7
            val units = TemperatureUnits.Celsius

            val expectedResult = APIResult.Success(createWeatherResponse())

            dataSource.setWeatherResponse(createWeatherResponse())

            val apiResult = repository.getDailyWeather(city, country, days, units)

            assertEquals(expectedResult, apiResult.first())
        }

    @Test
    fun `Given dataSource returns error, When getDailyWeather(), Then error is returned`() =
        runBlockingTest {
            val city = "Cairo"
            val country = "Egypt"
            val days = 7
            val units = TemperatureUnits.Celsius

            dataSource.setWeatherResponse(null)

            val apiResult = repository.getDailyWeather(city, country, days, units)

            assertThat(apiResult.first(), instanceOf(APIResult.Error::class.java))
        }

    @Test
    fun `Given iconName When buildIconUrl(), Then the correct icon url is returned `() {
        val iconName = "abc"
        val expectedUrl = "https://www.test.com/abc.png"
        val iconUrl = repository.buildIconUrl(iconName)

        assertEquals(expectedUrl, iconUrl)
    }

}