package com.android.weather.domain

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.android.weather.createWeatherDataItem
import com.android.weather.createWeatherResponse
import com.android.weather.data.WeatherRepository
import com.android.weather.data.model.WeatherResponse
import com.base.ErrorTypes
import com.base.Resource
import com.base.UserMessage
import com.remote.APIResult
import io.mockk.every
import io.mockk.mockk
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule

import org.junit.Test

@ExperimentalCoroutinesApi
class GetDailyWeatherUseCaseTest {

    private val dispatcher = TestCoroutineDispatcher()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    lateinit var useCase: GetDailyWeatherUseCase

    private val repository = mockk<WeatherRepository>()

    @Before
    fun setUp() {
        Dispatchers.setMain(dispatcher)
        useCase = GetDailyWeatherUseCase(repository)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `Given repository returns success response When  Then getWeatherForNextSevenDays, returns success data items `() =
        runBlockingTest {
            val city = "Cairo"
            val country = "Egypt"

            val apiResult = APIResult.Success(createWeatherResponse())
            val weatherDataItem = createWeatherDataItem()
            every { repository.getDailyWeather(any(), any(), any(), any()) } returns
                    flowOf(apiResult)

            val result = useCase.getWeatherForNextSevenDays(city, country)

            assertEquals(weatherDataItem, result.first().data)
        }

    @Test
    fun `Given repository returns error response When getWeatherForNextSevenDays(), Then getWeatherForNextSevenDays returns error  `() =
        runBlockingTest {
            val city = "Cairo"
            val country = "Egypt"

            val apiResult = APIResult.Error(ErrorTypes.GeneralError(UserMessage()))
            every { repository.getDailyWeather(any(), any(), any(), any()) } returns
                    flowOf(apiResult)

            val result = useCase.getWeatherForNextSevenDays(city, country)

            assertThat(result.first(), instanceOf(Resource.Error::class.java))
        }

    @Test
    fun `Given repository returns empty data When getWeatherForNextSevenDays(), Then getWeatherForNextSevenDays returns NoData error `() =
        runBlockingTest {
            val city = "Cairo"
            val country = "Egypt"

            val apiResult = APIResult.Success(WeatherResponse(emptyList(), city, "", ""))
            every { repository.getDailyWeather(any(), any(), any(), any()) } returns
                    flowOf(apiResult)

            val result = useCase.getWeatherForNextSevenDays(city, country)

            assertThat(
                (result.first() as Resource.Error).errorTypes,
                instanceOf(ErrorTypes.NoData::class.java)
            )
        }

}
