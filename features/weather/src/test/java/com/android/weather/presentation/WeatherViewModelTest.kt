package com.android.weather.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.asLiveData
import com.android.weather.createWeatherItem
import com.android.weather.domain.GetDailyWeatherUseCase
import com.android.weather.domain.model.WeatherDataItem
import com.base.ErrorTypes
import com.base.Resource
import com.base.UserMessage
import com.jraska.livedata.test
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before

import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class WeatherViewModelTest {

    private val dispatcher = TestCoroutineDispatcher()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    lateinit var viewModel: WeatherViewModel

    private val getDailyWeatherUseCase = mockk<GetDailyWeatherUseCase>()

    private val savedStateHandle = SavedStateHandle().apply {
        set(WeatherViewModel.CITY_ARG, "cairo")
        set(WeatherViewModel.COUNTRY_ARG, "egypt")
    }

    @Before
    fun setUp() {
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `Given useCase return success data, When viewModel is initialized Then flow result is Success with correct data `() =
        runBlockingTest {
            val weatherDataItem = WeatherDataItem(
                listOf(
                    createWeatherItem(),
                    createWeatherItem(),
                )
            )
            val successResource =
                Resource.Success(weatherDataItem)
            every {
                getDailyWeatherUseCase.getWeatherForNextSevenDays(
                    any(),
                    any()
                )
            } returns flowOf(successResource)


            viewModel = WeatherViewModel(getDailyWeatherUseCase, savedStateHandle)
            val testLiveData = viewModel.dailyWeatherStateFlow.asLiveData().test()

            testLiveData.assertValue(successResource)
        }

    @Test
    fun `Given useCase return error, When viewModel is initialized Then flow result is error `() =
        runBlockingTest {

            val error =
                Resource.Error<WeatherDataItem>(ErrorTypes.GeneralError(UserMessage(strMessage = "Error Happened")))

            every {
                getDailyWeatherUseCase.getWeatherForNextSevenDays(
                    any(),
                    any()
                )
            } returns flowOf(error)


            viewModel = WeatherViewModel(getDailyWeatherUseCase, savedStateHandle)
            val testLiveData = viewModel.dailyWeatherStateFlow.asLiveData().test()

            testLiveData.assertValue(error)
        }

    @Test
    fun `Given viewModel initialized and response hasn't returned, When onSwipeRefresh Then useCase will not be called `() =
        runBlockingTest {

            dispatcher.pauseDispatcher()
            coEvery {
                getDailyWeatherUseCase.getWeatherForNextSevenDays(
                    any(),
                    any()
                )
            } coAnswers {
                flow { Resource.Error<WeatherDataItem>(ErrorTypes.ConnectError()) }
            }

            viewModel = WeatherViewModel(getDailyWeatherUseCase, savedStateHandle)

            viewModel.onSwipeRefresh()

            dispatcher.resumeDispatcher()
            verify(exactly = 1) { getDailyWeatherUseCase.getWeatherForNextSevenDays(any(), any()) }
        }

}
