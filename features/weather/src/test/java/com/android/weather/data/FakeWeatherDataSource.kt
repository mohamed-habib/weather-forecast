package com.android.weather.data

import com.android.weather.createWeatherResponse
import com.android.weather.data.model.WeatherResponse
import com.android.weather.data.remote.IWeatherDataSource
import com.base.ErrorTypes
import com.base.UserMessage
import com.remote.APIResult
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

class FakeWeatherDataSource : IWeatherDataSource {

    private var weatherResponseFlow: Flow<APIResult<WeatherResponse>> = flowOf(
        APIResult.Success(
            createWeatherResponse()
        )
    )

    override fun getDailyWeather(
        city: String,
        country: String,
        days: Int,
        units: String
    ): Flow<APIResult<WeatherResponse>> {
        return weatherResponseFlow
    }

    override fun getIconBaseUrl(): String {
        return "https://www.test.com/"
    }

    /**
     * This function determines the return result of getDailyWeather(), should it be success or error
     */
    fun setWeatherResponse(weatherResponse: WeatherResponse?) {
        if (weatherResponse == null) weatherResponseFlow = flowOf(
            APIResult.Error(
                ErrorTypes.GeneralError(
                    UserMessage()
                )
            )
        )
        else flowOf(APIResult.Success(weatherResponse))
    }

}