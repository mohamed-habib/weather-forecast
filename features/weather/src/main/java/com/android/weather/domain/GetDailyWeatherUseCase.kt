package com.android.weather.domain

import com.android.uicomponents.utils.DateUtils
import com.android.weather.data.model.WeatherResponse
import com.android.weather.domain.model.WeatherDataItem
import com.android.weather.domain.model.WeatherItem
import com.base.ErrorTypes
import com.base.Resource
import com.remote.APIResult
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class GetDailyWeatherUseCase @Inject constructor(private val weatherRepository: IWeatherRepository) {


    fun getWeatherForNextSevenDays(
        city: String,
        country: String
    ): Flow<Resource<WeatherDataItem>> {
        val days = 7
        return weatherRepository.getDailyWeather(city, country, days, getTemperatureUnit())
            .map { weatherResponseApiResult ->
                when (weatherResponseApiResult) {
                    is APIResult.Success -> {
                        if (hasData(weatherResponseApiResult)) {
                            Resource.Error(ErrorTypes.NoData())
                        } else {
                            val weatherDataItem =
                                mapToDataItem(weatherResponseApiResult.data)
                            Resource.Success(weatherDataItem)
                        }
                    }
                    is APIResult.Error -> Resource.Error(weatherResponseApiResult.errorTypes)
                }
            }
    }

    private fun hasData(weatherResponseApiResult: APIResult.Success<WeatherResponse>) =
        weatherResponseApiResult.data == null || weatherResponseApiResult.data?.data.isNullOrEmpty()

    private fun mapToDataItem(response: WeatherResponse?): WeatherDataItem {
        val weatherDetailsList = response?.data?.map {
            val iconUrl = buildIconUrl(it.weather.icon)
            val dayOfWeek = DateUtils.getDayOfWeek(it.timeStamp)
            val uiDate = DateUtils.getUIDate(it.timeStamp)
            val temperature = it.temperature + getTemperatureUnit().uiUnit

            WeatherItem(
                temperature,
                it.relativeHumidity,
                it.precipitationProbability,
                iconUrl,
                it.weather.description,
                dayOfWeek,
                uiDate
            )
        }.orEmpty()
        return WeatherDataItem(weatherDetailsList)
    }

    private fun getTemperatureUnit(): TemperatureUnits {
        return TemperatureUnits.Fahrenheit
    }

    private fun buildIconUrl(iconName: String): String {
        if (iconName.isEmpty()) return ""
        return weatherRepository.buildIconUrl(iconName)
    }

}
