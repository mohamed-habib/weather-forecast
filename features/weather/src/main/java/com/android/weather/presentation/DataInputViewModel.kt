package com.android.weather.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.android.weather.R
import com.base.ErrorTypes
import com.base.Resource
import com.base.SingleLiveEvent
import com.base.UserMessage
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DataInputViewModel @Inject constructor() : ViewModel() {

    private val _inputValidationFlow = SingleLiveEvent<Resource<UserMessage?>>()
    val inputValidationFlow: LiveData<Resource<UserMessage?>> = _inputValidationFlow

    fun validateDataInput(city: String?, country: String?) {
        _inputValidationFlow.value =
            when {
                city.isNullOrEmpty() -> Resource.Error(
                    ErrorTypes.GeneralError(
                        UserMessage(
                            resMessage = R.string.enter_city
                        )
                    )
                )
                country.isNullOrEmpty() -> Resource.Error(
                    ErrorTypes.GeneralError(
                        UserMessage(
                            resMessage = R.string.enter_country
                        )
                    )
                )
                else -> Resource.Success(null)
            }
    }

}
