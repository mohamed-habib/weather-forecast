package com.android.weather.presentation

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.asLiveData
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.weather.R
import com.base.BaseFragment
import com.android.weather.databinding.FragmentWeatherDataBinding
import com.base.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WeatherDataFragment :
    BaseFragment<FragmentWeatherDataBinding>(FragmentWeatherDataBinding::inflate) {
    private val viewModel: WeatherViewModel by viewModels()
    private val weatherAdapter: WeatherAdapter by lazy {
        WeatherAdapter()
    }

    private val args: WeatherDataFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setData()
        setupRecyclerView()
        binding.swipeRefresh.setOnRefreshListener {
            viewModel.onSwipeRefresh()
        }
        observers()
    }

    private fun setData() {
        binding.cityTextView.text = args.city
        binding.countryTextView.text = args.country
    }

    private fun setupRecyclerView() {
        with(binding.weatherDataRecyclerView) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = weatherAdapter
        }
    }

    private fun observers() {
        viewModel.dailyWeatherStateFlow.asLiveData()
            .observe(viewLifecycleOwner) { weatherDataItemResource ->
                when (weatherDataItemResource) {
                    is Resource.Success -> {
                        hideLoading()
                        binding.errorTextView.isVisible = false
                        weatherAdapter.addItems(weatherDataItemResource.data?.weatherListItems.orEmpty())
                    }
                    is Resource.Error -> {
                        hideLoading()
                        binding.errorTextView.text =
                            weatherDataItemResource.errorTypes.errorMessage?.getMessage(
                                requireContext()
                            ) ?: getString(R.string.error_happened)
                    }
                    is Resource.Loading -> showLoading()
                }
            }
    }

    private fun showLoading() {
        binding.progressBar.isVisible = true
    }

    private fun hideLoading() {
        binding.progressBar.isVisible = false
        binding.swipeRefresh.isRefreshing = false
    }

}
