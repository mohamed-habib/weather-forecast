package com.android.weather.presentation

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.weather.databinding.ItemWeatherBinding
import com.android.weather.domain.model.WeatherItem
import com.bumptech.glide.Glide

class WeatherAdapter : RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder>() {

    private var data = mutableListOf<WeatherItem>()
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        val binding = ItemWeatherBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        context = parent.context
        return WeatherViewHolder(binding)
    }

    fun addItems(weatherList: List<WeatherItem>) {
        data = weatherList.toMutableList()
        notifyDataSetChanged()
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) =
        holder.bind(data[position])

    inner class WeatherViewHolder(private val binding: ItemWeatherBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(weatherItem: WeatherItem) {
            binding.dayTextView.text = weatherItem.dayOfWeek
            binding.dateTextView.text = weatherItem.date
            Glide.with(context)
                .load(weatherItem.iconUrl)
                .centerCrop()
                .into(binding.iconImageView)
            binding.temperatureTextView.text = weatherItem.temperature
            binding.humidityTextView.text = weatherItem.relativeHumidity
            binding.conditionTextView.text = weatherItem.description
            binding.preceptionChanceTextView.text = weatherItem.precipitationProbability
        }

    }
}


