package com.android.weather.presentation

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.android.weather.R
import com.android.weather.databinding.FragmentUserDataBinding
import com.base.BaseFragment
import com.base.Resource
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DataInputFragment :
    BaseFragment<FragmentUserDataBinding>(FragmentUserDataBinding::inflate) {

    private val viewModel: DataInputViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        clickListeners()
        observers()
    }

    private fun observers() {
        viewModel.inputValidationFlow.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    val city = binding.cityEditText.text.toString()
                    val country = binding.countryEditText.text.toString()

                    findNavController().navigate(
                        DataInputFragmentDirections.actionFromDataInputToWeatherData(
                            city,
                            country
                        )
                    )
                }
                is Resource.Error -> {
                    showSnackBar(
                        binding.root,
                        it.errorTypes.errorMessage?.getMessage(requireContext())
                            ?: getString(R.string.error_happened)
                    )
                }
            }
        }

    }

    private fun showSnackBar(view: View, message: String) {
        Snackbar.make(
            view,
            message,
            Snackbar.LENGTH_LONG
        ).show()
    }

    private fun clickListeners() {
        binding.goButton.setOnClickListener {
            val city = binding.cityEditText.text.toString()
            val country = binding.countryEditText.text.toString()
            viewModel.validateDataInput(city, country)
        }
    }

}
