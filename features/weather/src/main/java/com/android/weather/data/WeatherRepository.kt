package com.android.weather.data

import com.android.weather.data.model.WeatherResponse
import com.android.weather.data.remote.IWeatherDataSource
import com.android.weather.domain.IWeatherRepository
import com.android.weather.domain.TemperatureUnits
import com.remote.APIResult
import kotlinx.coroutines.flow.Flow

class WeatherRepository(private val weatherDataSource: IWeatherDataSource) : IWeatherRepository {
    companion object {
        const val ICON_EXTENSION = ".png"
    }

    override fun getDailyWeather(
        city: String,
        country: String,
        days: Int,
        units: TemperatureUnits
    ): Flow<APIResult<WeatherResponse>> {
        return weatherDataSource.getDailyWeather(city, country, days, units.serverUnit)
    }

    override fun buildIconUrl(iconName: String): String {
        return weatherDataSource.getIconBaseUrl() + iconName + ICON_EXTENSION
    }

}
