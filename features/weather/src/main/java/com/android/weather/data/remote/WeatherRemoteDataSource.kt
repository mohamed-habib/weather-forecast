package com.android.weather.data.remote

import com.android.remote.BuildConfig
import com.android.weather.data.model.WeatherResponse
import com.remote.APIResult
import com.remote.BaseRemoteDataSource
import kotlinx.coroutines.flow.Flow

class WeatherRemoteDataSource(private val weatherAPI: DailyWeatherAPI) : BaseRemoteDataSource(),
    IWeatherDataSource {
    companion object {
        const val CITY_QUERY = "city"
        const val COUNTRY_QUERY = "country"
        const val DAYS_QUERY = "days"
        const val UNITS_QUERY = "units"
        const val KEY_QUERY = "key"
    }

    override fun getDailyWeather(
        city: String,
        country: String,
        days: Int,
        units: String,
    ): Flow<APIResult<WeatherResponse>> {
        val apiKey = BuildConfig.API_KEY

        val options = mapOf(
            CITY_QUERY to city,
            COUNTRY_QUERY to country,
            DAYS_QUERY to days.toString(),
            UNITS_QUERY to units,
            KEY_QUERY to apiKey,
        )
        return safeApiCall {
            weatherAPI.getDailyWeather(options)
        }
    }

    override fun getIconBaseUrl(): String {
        return BuildConfig.ICONS_BASE_URL
    }
}
