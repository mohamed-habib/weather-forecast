package com.android.weather.presentation

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.weather.domain.GetDailyWeatherUseCase
import com.android.weather.domain.model.WeatherDataItem
import com.base.Resource
import com.base.Resource.Loading
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class WeatherViewModel @Inject constructor(
    private val getDailyWeatherUseCase: GetDailyWeatherUseCase,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    companion object {
        const val CITY_ARG = "city"
        const val COUNTRY_ARG = "country"
    }

    private val _dailyWeatherStateFlow =
        MutableStateFlow<Resource<WeatherDataItem>>(Resource.Idle())
    val dailyWeatherStateFlow: StateFlow<Resource<WeatherDataItem>> = _dailyWeatherStateFlow

    private var dailyWeatherJob: Job? = null

    private val city = savedStateHandle.get<String>(CITY_ARG).orEmpty()
    private val country = savedStateHandle.get<String>(COUNTRY_ARG).orEmpty()

    init {
        getDailyWeather(city, country)
    }

    private fun getDailyWeather(city: String, country: String) {
        //to make sure not to send a new request, until the previous one is complete
        if (dailyWeatherJob == null || dailyWeatherJob?.isCompleted == true)
            dailyWeatherJob = getDailyWeatherUseCase.getWeatherForNextSevenDays(city, country)
                .onStart { Loading<Resource<WeatherDataItem>>(true) }
                .map { weatherDataItemResource ->
                    _dailyWeatherStateFlow.value = weatherDataItemResource
                }.launchIn(viewModelScope)
    }

    fun onSwipeRefresh() {
        getDailyWeather(city, country)
    }

}
