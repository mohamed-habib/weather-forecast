package com.android.weather.data.remote

import com.android.weather.data.model.WeatherResponse
import com.remote.APIResult
import kotlinx.coroutines.flow.Flow

interface IWeatherDataSource {
    fun getDailyWeather(
        city: String,
        country: String,
        days: Int,
        units: String
    ): Flow<APIResult<WeatherResponse>>

    fun getIconBaseUrl(): String
}
