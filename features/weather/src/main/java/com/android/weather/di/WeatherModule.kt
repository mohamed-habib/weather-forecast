package com.android.weather.di

import com.android.weather.data.WeatherRepository
import com.android.weather.data.remote.DailyWeatherAPI
import com.android.weather.data.remote.IWeatherDataSource
import com.android.weather.data.remote.WeatherRemoteDataSource
import com.android.weather.domain.IWeatherRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import retrofit2.Retrofit

@Module
@InstallIn(ViewModelComponent::class)
object WeatherModule {

    @Provides
    fun provideWeatherAPI(retrofit: Retrofit): DailyWeatherAPI {
        return retrofit.create(DailyWeatherAPI::class.java)
    }

    @Provides
    fun provideWeatherRemoteDataSource(weatherAPI: DailyWeatherAPI): IWeatherDataSource {
        return WeatherRemoteDataSource(weatherAPI)
    }

    @Provides
    fun provideWeatherRepository(weatherRemoteDataSource: IWeatherDataSource): IWeatherRepository {
        return WeatherRepository(weatherRemoteDataSource)
    }

}
