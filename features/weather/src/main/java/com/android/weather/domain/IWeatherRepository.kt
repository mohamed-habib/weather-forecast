package com.android.weather.domain

import com.android.weather.data.model.WeatherResponse
import com.remote.APIResult
import kotlinx.coroutines.flow.Flow

interface IWeatherRepository {

    fun getDailyWeather(
        city: String,
        country: String,
        days: Int,
        units: TemperatureUnits
    ): Flow<APIResult<WeatherResponse>>

    fun buildIconUrl(iconName: String): String
}
