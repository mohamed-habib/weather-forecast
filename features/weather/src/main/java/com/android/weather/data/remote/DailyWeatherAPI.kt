package com.android.weather.data.remote

import com.android.weather.data.model.WeatherResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface DailyWeatherAPI {
    @GET("daily")
    suspend fun getDailyWeather(@QueryMap options: Map<String, String>): Response<WeatherResponse>
}
