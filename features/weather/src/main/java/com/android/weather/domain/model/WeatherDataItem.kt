package com.android.weather.domain.model

data class WeatherDataItem(
    val weatherListItems: List<WeatherItem>
)

data class WeatherItem(
    val temperature: String,
    val relativeHumidity: String,
    val precipitationProbability: String,
    val iconUrl: String,
    val description: String,
    val dayOfWeek: String,
    val date: String
)
