package com.android.weather.domain

enum class TemperatureUnits(val uiUnit: String, val serverUnit: String) {
    Celsius("C", "M"),
    Fahrenheit("F", "I");
}