package com.android.weather.data.model

import com.google.gson.annotations.SerializedName


data class WeatherResponse(
    @SerializedName("data")
    val data: List<WeatherDetails>?,
    @SerializedName("cityName")
    val cityName: String,
    @SerializedName("lat")
    val lat: String,
    @SerializedName("lon")
    val lon: String
)

data class WeatherDetails(
    @SerializedName("temp")
    val temperature: String,
    @SerializedName("rh")
    val relativeHumidity: String,
    @SerializedName("weather")
    val weather: Weather,
    @SerializedName("pop")
    val precipitationProbability: String,
    @SerializedName("ts")
    val timeStamp: Long
)

data class Weather(
    @SerializedName("icon")
    val icon: String,
    @SerializedName("description")
    val description: String
)
