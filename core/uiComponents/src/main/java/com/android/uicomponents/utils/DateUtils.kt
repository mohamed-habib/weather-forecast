package com.android.uicomponents.utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    private const val UI_FORMAT = "dd/MM/yyy"

    fun getDayOfWeek(timeStamp: Long): String {
        val sdf = SimpleDateFormat("EEEE", Locale.getDefault())
        return sdf.format(Date(timeStamp * 1000))
    }

    fun getUIDate(timeStamp: Long): String {
        val sdf = SimpleDateFormat(UI_FORMAT, Locale.getDefault())
        return sdf.format(Date(timeStamp * 1000))
    }
}
