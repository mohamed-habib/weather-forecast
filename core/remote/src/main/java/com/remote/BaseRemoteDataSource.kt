package com.remote

import com.base.ErrorTypes
import com.base.UserMessage
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Response
import java.net.UnknownHostException

open class BaseRemoteDataSource {
    fun <T : Any> safeApiCall(
        call: suspend () -> Response<T>
    ): Flow<APIResult<T>> =
        flow {
            emit(safeApiResult(call))
        }

    private suspend fun <T : Any> safeApiResult(call: suspend () -> Response<T>): APIResult<T> {
        var response: Response<T>? = null
        try {

            response = call.invoke()

        } catch (ex: Exception) {
            ex.printStackTrace()
            return getResultError(response, ex)
        }

        if (response.isSuccessful) {
            return APIResult.Success(response.body())
        }

        return getResultError(response)
    }

    private fun <T> getResultError(response: Response<T>?, ex: Exception? = null): APIResult.Error {
        if (ex is UnknownHostException) {
            return APIResult.Error(ErrorTypes.ConnectError())
        }
        return APIResult.Error(
            ErrorTypes.NetworkError(
                UserMessage(strMessage = response?.errorBody()?.string().orEmpty()),
                response?.code().toString()
            )
        )
    }

}
