package com.base

// A generic class that contains data and status about loading this data.
sealed class Resource<T>(
    val data: T? = null
) {
    class Success<T>(data: T) : Resource<T>(data)
    class Loading<T>(val show: Boolean) : Resource<T>()
    class Error<T>(val errorTypes: ErrorTypes) : Resource<T>()
    class Idle<T> : Resource<T>()
}